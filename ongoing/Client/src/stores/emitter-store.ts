import { observable, action } from "mobx";

export class EmitterStore {
    @observable ModalObjectiveIsOpen = false;
    @observable FilterIsOpen = false;
    @observable ModalCardIsOpen = false;

    @action
    toggleModalObjective() {
        this.ModalObjectiveIsOpen = !this.ModalObjectiveIsOpen;
    }

    @action
    toggleFilter() {
        this.FilterIsOpen = !this.FilterIsOpen;
    }

    @action.bound
    toggleModalCard() {
        this.ModalCardIsOpen = !this.ModalCardIsOpen;
    }
}
