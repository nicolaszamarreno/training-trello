import { KeyResult } from "./objectives-store";
import { observable, action } from "mobx";

export interface KeyResult {
    id: number;
    title: string;
    tag: string;
    tagColor: string;
    deadline: string;
    comments: string;
    tags: string[];
}

export interface Objective {
    id: number;
    title: string;
    target: number;
    currentTarget: number;
    isPrivate: boolean;
    colorObjective: string;
    deadline?: Date;
    keyResults: KeyResult[];
}

export class ObjectivesStore {
    @observable public objectives: Objective[] = [];

    @action
    changeOrderCard(dragIndex: number, hoverIndex: number, idObjective: number) {
        let oldKeyResult = this.objectives[idObjective].keyResults;

        const arrayMove = (arr: KeyResult[], fromIndex: number, toIndex: number) => {
            const element = arr[fromIndex];
            arr.splice(fromIndex, 1);
            arr.splice(toIndex, 0, element);

            return arr;
        };

        this.objectives[idObjective].keyResults = arrayMove(oldKeyResult, hoverIndex, dragIndex);
    }

    @action
    getAllObjectives() {
        fetch("http://localhost:50948/api/tag/Nou", { method: "GET" })
            .then(response => response.json())
            .then(response => console.log(response));
        return fetch("http://localhost:50948/api/objective", { method: "GET" }).then(response => response.json());
    }

    @action
    addObjective(titleObjective: string, descriptionObjective: string, targetObjective: number) {
        let idNewObjective = this.objectives.length + 1;

        this.objectives.push({
            id: idNewObjective,
            colorObjective: "#31B87B",
            title: titleObjective,
            isPrivate: true,
            currentTarget: 80,
            target: targetObjective,
            keyResults: []
        });
    }

    @action
    changeObjectivePercent(percent: number) {
        this.objectives[0].target = percent;
    }

    @action
    SetObjectives(objectives: Objective[]) {
        this.objectives = objectives;
    }

    @action.bound
    addKeyResult(objectiveId: number, title: string) {
        fetch(`http://localhost:50948/api/keyresult/`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                IdObjective: objectiveId,
                title: title
            })
        })
            .then(response => response.json())
            .then(response => {
                this.objectives[objectiveId - 1].keyResults.push({
                    id: response,
                    title: title,
                    tag: "",
                    tagColor: "",
                    deadline: "",
                    comments: "",
                    tags: ["Bonjour", "aurevoir"]
                });
            });
    }

    @action.bound
    changeNameOfKeyResult(keyResult: KeyResult, title: string) {
        keyResult.title = title;
    }
}
