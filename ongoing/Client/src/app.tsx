import * as React from "react";
import { Board } from "./components/organisms/board";
import { Header } from "./components/organisms/header";
import { TSNavbar } from "./components/organisms/ts-navbar";
import { EmitterStore } from "./stores/emitter-store";
import { inject, observer } from "mobx-react";
import { Filter } from "./components/organisms/filter";
import { PanelObjective } from "./components/organisms/panel-objective";
import { ObjectivesStore } from "./stores/objectives-store";

export interface AppProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}

export interface AppState {
    isLoading: boolean;
}

@inject("emitterStore", "objectivesStore")
@observer
export class App extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);

        this.state = {
            isLoading: false
        };
    }

    componentDidMount() {
        this.props.objectivesStore!.getAllObjectives().then(response => {
            this.props.objectivesStore!.SetObjectives(response);
            this.setState({
                isLoading: true
            });
        });
    }

    public render() {
        if (this.state.isLoading) {
            return (
                <div>
                    <TSNavbar />
                    <div className="ongoing">
                        {this.props.emitterStore!.ModalObjectiveIsOpen && <PanelObjective />}
                        <Header />
                        <main className="ongoing__wrapper">
                            <Filter />
                            <Board />
                        </main>
                    </div>
                </div>
            );
        } else {
            return <div>chargement</div>;
        }
    }
}
