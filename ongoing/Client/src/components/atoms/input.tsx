import * as React from "react";

export type InputTypes = "text" | "number" | "mail";

export interface InputProps {
    type: InputTypes;
    pattern?: string;
    componentClass?: string;
    idControl?: string;
    placeholder?: string;
    value?: string;
    required?: boolean;
    onChange?: () => void;
    onKeyPress?: () => void;
    inputValid?: boolean;
    warningMessage?: string;
    icon?: string;
    iconClass?: string;
}

class Input extends React.Component<InputProps, {}> {
    constructor(props: InputProps) {
        super(props);
    }

    renderMessage() {}

    render() {
        return (
            <div className="input-text__box">
                <div className="input-text__wrapper">
                    {this.props.icon && (
                        <i
                            className={`input-text__icon ${this.props.icon} ${this.props.iconClass ? this.props.iconClass : ""}`}
                        />
                    )}

                    <input
                        className={`input-text ${!this.props.inputValid ? "input-text--warning" : ""} ${
                            this.props.icon ? "input-text--icon" : ""
                        } ${this.props.componentClass ? this.props.componentClass : ""}`}
                        type={this.props.type}
                        id={this.props.idControl}
                        placeholder={this.props.placeholder}
                        value={this.props.value}
                        required={this.props.required}
                        name={this.props.idControl}
                        pattern={this.props.pattern}
                        onChange={this.props.onChange}
                        onKeyPress={this.props.onKeyPress}
                    />
                    <span className="input-text__status-bar" />
                </div>
                {!this.props.inputValid && <span className="input__message">{this.props.warningMessage}</span>}
            </div>
        );
    }
}

export { Input };
