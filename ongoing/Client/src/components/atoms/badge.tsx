import * as React from "react";
export type BadgeType = "header" | "counter" | "date";

export interface BadgeProps {
    type: BadgeType;
    value?: number;
    componentClass?: string;
    color?: string;
    month?: string;
}

export class Badge extends React.Component<BadgeProps, {}> {
    constructor(props: BadgeProps) {
        super(props);
    }

    public render() {
        if (this.props.type === "header" && this.props.value !== null) {
            const badgeColor = this.props.value! > 2 ? "ruban" : "smart";

            return (
                <span
                    className={`${
                        this.props.componentClass ? this.props.componentClass : ""
                    } badge--head background-color--${badgeColor}`}
                >
                    {this.props.value}
                </span>
            );
        } else if (this.props.type === "date") {
            return (
                <span className="badge badge--date">
                    <strong>{this.props.value}</strong>
                    <span>{this.props.month}</span>
                </span>
            );
        } else {
            return <span className="button__badge badge--counter">{this.props.value}</span>;
        }
    }
}
