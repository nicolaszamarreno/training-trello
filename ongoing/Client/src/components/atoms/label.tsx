import * as React from "react";

export interface LabelProps {
    idControl?: string;
    classComponent?: string;
}

class Label extends React.Component<LabelProps, {}> {
    render() {
        return (
            <label
                className={`label ${this.props.classComponent ? this.props.classComponent : ""}`}
                htmlFor={this.props.idControl}
            >
                {this.props.children}
            </label>
        );
    }
}

export { Label };
