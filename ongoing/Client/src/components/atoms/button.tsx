import * as React from "react";
import { Badge } from "./badge";

export type ButtonStatus = "default" | "success" | "danger" | "warning" | "disable";
export type ButtonSize = "small" | "medium" | "normal";
export type ButtonRole = "button" | "reset" | "submit";
export type ButtonStyle = "primary" | "secondary" | "round";
export type ButtonColor = "smart" | "stormtrooper";

export interface ButtonProps {
    style: ButtonStyle;
    size?: ButtonSize;
    label?: string;
    badgeValue?: number;
    status?: ButtonStatus;
    role?: ButtonRole;
    color?: ButtonColor;
    backgroundColor?: ButtonColor;
    borderColor?: ButtonColor;
    icon?: string;
    link?: string;
    componentClass?: string;
    onClick?: () => void;
}

export class Button extends React.Component<ButtonProps, {}> {
    constructor(props: ButtonProps) {
        super(props);
    }

    public getClasses(): string[] {
        const properties: string[] = [];
        properties.push(`button-${this.props.style}`);
        this.props.size ? properties.push(`button-${this.props.style}--${this.props.size}`) : "";
        this.props.status ? properties.push(`button-${this.props.style}--${this.props.status}`) : "";
        this.props.componentClass ? properties.push(this.props.componentClass) : "";
        this.props.backgroundColor ? properties.push(`background-color--${this.props.backgroundColor}`) : "";
        this.props.borderColor ? properties.push(`border-color--${this.props.borderColor}`) : "";
        this.props.color ? properties.push(`color--${this.props.color}`) : "";

        return properties;
    }

    public render() {
        if (!this.props.link) {
            return (
                <button
                    title={this.props.label}
                    role={this.props.role}
                    onClick={this.props.onClick}
                    className={`button ${this.getClasses().join(" ")}`}
                    disabled={this.props.status === "disable"}
                >
                    {this.props.icon && <i className={`${this.props.icon}`} />}
                    {this.props.children}
                    {this.props.badgeValue && <Badge type="counter" value={this.props.badgeValue} />}
                </button>
            );
        } else {
            return (
                <a
                    href={this.props.link}
                    title={this.props.label}
                    onClick={this.props.onClick}
                    className={`button ${this.getClasses().join(" ")}`}
                >
                    {this.props.icon && <i className={`${this.props.icon}`} />}
                    {this.props.children}
                    {this.props.badgeValue && <Badge type="counter" value={this.props.badgeValue} />}
                </a>
            );
        }
    }
}
