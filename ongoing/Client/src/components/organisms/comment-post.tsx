import * as React from "react";
import { ButtonMenu } from "../molecules/button-menu";

export interface Comment {
    comment: string;
    author: string;
    time?: string;
    date?: string;
}

export interface CommentPostProps {
    comments: Comment[];
}

export class CommentPost extends React.Component<CommentPostProps, {}> {
    constructor(props: CommentPostProps) {
        super(props);
    }

    render() {
        return (
            <div className="comment-post">
                <span className="comment-post__date badge badge--date">
                    <strong>28</strong> <span>OCT</span>
                </span>
                <div className="comment-post__wrapper">
                    <ul className="comment-post__list">
                        {this.props.comments.map((item, key) => {
                            return (
                                <li key={key} className="comment-post__item">
                                    <div className="comment-post__avatar avatar avatar--feed">TS</div>
                                    <div className="comment-post__message" data-date="28 OCT">
                                        <div className="comment-post__header">
                                            <p className="comment-post__time">9:24</p>
                                            <p className="comment-post__name">{item.author}</p>
                                            <ButtonMenu
                                                style="round"
                                                icon="icon-more"
                                                componentClass="comment-post__button"
                                                actionsList={[
                                                    {
                                                        color: "color--smart",
                                                        iconClass: "icon-edit",
                                                        label: "Edit"
                                                    },
                                                    {
                                                        color: "color--ruban",
                                                        iconClass: "icon-trash",
                                                        label: "Delete"
                                                    }
                                                ]}
                                            />
                                        </div>
                                        <p className="comment-post__text">{item.comment}</p>
                                    </div>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}
