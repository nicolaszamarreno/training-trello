import * as React from "react";
import { Card } from "../molecules/card";
import HTML5Backend from "react-dnd-html5-backend";
import { DragDropContext } from "react-dnd";
import { inject, observer } from "mobx-react";
import { KeyResult, ObjectivesStore } from "../../stores/objectives-store";
import { ColumnHeader, ColumnHeaderProps } from "../molecules/column-header";
import { PopinAddObjective } from "./popin-add-objective";
import { CardAdd } from "../molecules/card-add";

export interface ColumnProps extends ColumnHeaderProps {
    idObjective: number;
    keyResults?: KeyResult[];
    objectivesStore?: ObjectivesStore;
}

export interface ColumnState {
    displayModal: boolean;
    addMode: boolean;
    keyResultIsSelected: KeyResult | null;
}

@inject("objectivesStore")
@observer
class Column extends React.Component<ColumnProps, ColumnState> {
    constructor(props: ColumnProps) {
        super(props);

        this.state = {
            displayModal: false,
            addMode: false,
            keyResultIsSelected: null
        };
    }

    moveCard(dragIndex: number, hoverIndex: number, indexObjective: number) {
        this.props.objectivesStore!.changeOrderCard(dragIndex, hoverIndex, indexObjective);
    }

    toggleAddCard() {
        this.setState({
            addMode: !this.state.addMode
        });
    }

    hideModalKeyResult() {
        this.setState({
            displayModal: false
        });
    }

    showModalKeyResult(keyResult: KeyResult) {
        this.setState({
            displayModal: true,
            keyResultIsSelected: keyResult
        });
    }

    public render() {
        const { keyResults } = this.props;
        return (
            <div className="column">
                {this.state.displayModal &&
                    this.state.keyResultIsSelected !== null && (
                        <PopinAddObjective
                            keyResult={this.state.keyResultIsSelected}
                            closePopin={() => this.hideModalKeyResult()}
                        />
                    )}
                <ColumnHeader
                    titleObjective={this.props.titleObjective}
                    indexObjective={this.props.indexObjective}
                    colorObjective={this.props.colorObjective}
                    deadline={this.props.deadline}
                    comments={this.props.comments}
                    isPrivate={this.props.isPrivate}
                />
                <div className="column__wrapper">
                    {keyResults!.length > 0 &&
                        keyResults!.map((card, index) => {
                            return (
                                <Card
                                    keyResult={card}
                                    key={card.id}
                                    index={index}
                                    id={card.id}
                                    indexObjective={this.props.indexObjective}
                                    name={card.title}
                                    tag={card.tag}
                                    tagColor={card.tagColor}
                                    comments={card.comments}
                                    deadline={card.deadline}
                                    onClick={keyResult => this.showModalKeyResult(keyResult)}
                                    moveCard={(dragIndex, hoverIndex, indexObjective) =>
                                        this.moveCard(dragIndex, hoverIndex, indexObjective)
                                    }
                                />
                            );
                        })}
                    {this.state.addMode && (
                        <CardAdd
                            idObjective={this.props.idObjective}
                            indexObjective={this.props.indexObjective}
                            onCancel={() => this.toggleAddCard()}
                        />
                    )}
                </div>
                <div className="column__add" onClick={() => this.toggleAddCard()}>
                    Add key result
                </div>
            </div>
        );
    }
}

export default DragDropContext(HTML5Backend)(Column);
