import * as React from "react";

export interface MenuActionsProps {
    componentClass?: string;
    menuConfig?: ConfigButton;
}

export interface ConfigButton {
    componentClass?: string;
    borderColor?: string;
    color?: string;
    icon?: string;
}

export class MenuActions extends React.Component<MenuActionsProps, {}> {
    constructor(props: MenuActionsProps) {
        super(props);
    }

    render() {
        return (
            <ul className={`${this.props.componentClass ? this.props.componentClass : ""} menu-actions`}>
                {this.props.children}
            </ul>
        );
    }
}
