import * as React from "react";
import DatePicker from "react-datepicker";
import { Moment } from "moment";
import { TextareaLabel } from "../molecules/textarea-label";
import { Button } from "../atoms/button";
import { ButtonMenu } from "../molecules/button-menu";
import { observer, inject } from "mobx-react";
import { ObjectivesStore, KeyResult } from "../../stores/objectives-store";
import { AddTag } from "./add-tags";
const moment = require("moment");

export interface PopinAddObjectiveProps {
    closePopin: () => void;
    keyResult: KeyResult;
    objectivesStore?: ObjectivesStore;
}

export interface PopinAddObjectiveState {
    startDate: Moment;
    objectiveDescription: string;
    titleOnFocus: boolean;
}

@inject("objectivesStore")
@observer
export class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps) {
        super(props);

        this.state = {
            startDate: moment(),
            objectiveDescription: "",
            titleOnFocus: false
        };
    }

    componentDidMount() {
        document.body.addEventListener("keyup", keyCap => {
            if (keyCap.keyCode === 27) this.props.closePopin();
        });
    }

    componentWillUnmount() {
        document.body.removeEventListener("keyup", this.props.closePopin);
    }

    handleInputChangeFocus() {
        this.setState({
            titleOnFocus: !this.state.titleOnFocus
        });
    }

    handleChange(date: Moment) {
        this.setState({
            startDate: date
        });
    }

    handleModalName(title: string) {
        this.props.objectivesStore!.changeNameOfKeyResult(this.props.keyResult, title);
    }

    render() {
        return (
            <div className="popin__overlaid">
                <div className="modal-keyResult">
                    <header className="modal-keyResult__header">
                        <input
                            type="text"
                            className={`modal-keyResult__title ${
                                this.state.titleOnFocus ? "modal-keyResult__title--active" : ""
                            }`}
                            value={this.props.keyResult.title}
                            onChange={event => this.handleModalName(event.currentTarget.value)}
                            onFocus={() => this.handleInputChangeFocus()}
                            onBlur={() => this.handleInputChangeFocus()}
                        />
                        <ButtonMenu
                            style="round"
                            icon="icon-more"
                            componentClass="modal-keyResult__menu"
                            actionsList={[
                                {
                                    color: "color--smart",
                                    iconClass: "icon-files",
                                    label: "Copy"
                                },
                                {
                                    color: "color--smart",
                                    iconClass: "icon-delete",
                                    label: "Close"
                                },
                                {
                                    color: "color--ruban",
                                    iconClass: "icon-trash",
                                    label: "Delete"
                                }
                            ]}
                        />
                    </header>
                    <div className="modal-keyResult__wrapper">
                        <TextareaLabel TextareaPlaceholder="Your description" onChange={() => console.log("Change text")}>
                            Description
                        </TextareaLabel>

                        <div className="mt-20">
                            <span className="label">Due Date</span>
                            <label className="input-datepicker">
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange.bind(this)}
                                    className="input-datepicker__date"
                                    placeholderText="I have been cleared!"
                                />
                                <span className="input-datepicker__status-bar" />
                            </label>
                        </div>

                        <div className="mt-20">
                            <span className="label">Add tags</span>
                            <AddTag tags={["helo"]} />
                        </div>
                    </div>

                    <div className="modal-keyResult-footer">
                        <Button style="secondary" size="normal" status="danger" onClick={() => this.props.closePopin()}>
                            Close
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}
