import * as React from "react";
import { InputLabel } from "../molecules/input-label";
import { TextareaLabel } from "../molecules/textarea-label";
import { Button } from "../atoms/button";
import { EmitterStore } from "../../stores/emitter-store";
import { inject, observer } from "mobx-react";
import { Comment } from "./comment";
import { ScaleLine } from "./scale-line";
import { ObjectivesStore } from "../../stores/objectives-store";
import { InputDropdown } from "../molecules/input-dropdown";
import DatePicker from "react-datepicker";
import { Moment } from "moment";
import moment = require("moment");
import { InputSwitch } from "../molecules/input-switch";

export interface PanelObjectiveState {
    title: string;
    description: string;
    startDate: Moment;
}

export interface PanelObjectiveProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}

@inject("emitterStore", "objectivesStore")
@observer
export class PanelObjective extends React.Component<PanelObjectiveProps, PanelObjectiveState> {
    constructor(props: PanelObjectiveProps) {
        super(props);

        this.state = {
            title: "",
            description: "",
            startDate: moment()
        };
    }

    changeTitle(event: React.FormEvent<HTMLInputElement>) {
        this.setState({
            title: event.currentTarget.value
        });
    }

    changeDescription(event: React.FormEvent<HTMLInputElement>) {
        this.setState({
            description: event.currentTarget.value
        });
    }

    getIndexPointer(value: number) {
        this.props.objectivesStore!.changeObjectivePercent(value);
    }

    handleSubmit(event: React.FormEvent<HTMLInputElement>) {}

    handleChange(date: Moment) {
        this.setState({
            startDate: date
        });
    }

    public render() {
        const choices = [
            {
                label: "Personnal",
                active: false
            },
            {
                label: "Collective",
                active: true
            },
            {
                label: "Shared",
                active: false
            }
        ];
        return (
            <div className="panel-objective">
                <header className="panel-objective-header">
                    <div className="panel-objective-header__category" />
                    <div className="panel-objective-header__wrapper">
                        <h2>{this.state.title ? this.state.title : "Your Title"}</h2>
                        <Button
                            style="round"
                            backgroundColor="stormtrooper"
                            size="normal"
                            componentClass="panel-objective-header__buttonClose"
                            icon="icon-delete color--smart"
                            onClick={() => this.props.emitterStore!.toggleModalObjective()}
                        />
                    </div>
                </header>
                <div className="panel-objective__wrapper">
                    <div className="row panel-objective__layout">
                        <div className="col-lg-6 col-xs-12">
                            <div className="row">
                                <div className="col-lg-12 col-xs-12">
                                    <InputLabel
                                        inputType="text"
                                        inputPlaceholder="Your title"
                                        onChange={this.changeTitle.bind(this)}
                                        inputValid={true}
                                    >
                                        Label
                                    </InputLabel>
                                </div>
                            </div>
                            <div className="row panel-objective__row">
                                <div className="col-lg-12 col-xs-12">
                                    <TextareaLabel
                                        TextareaPlaceholder="Your description"
                                        onChange={this.changeDescription.bind(this)}
                                    >
                                        Description
                                    </TextareaLabel>
                                </div>
                            </div>

                            <div className="row panel-objective__row">
                                <div className="col-lg-12 col-xs-12 panel-objective__flex">
                                    <div className="panel-objective__target">
                                        <InputLabel
                                            inputType="text"
                                            inputPlaceholder="...%"
                                            onChange={this.changeTitle.bind(this)}
                                            inputValid={true}
                                        >
                                            Target
                                        </InputLabel>
                                    </div>
                                    <div className="panel-objective__datepicker">
                                        <span className="label">Due Date</span>
                                        <label className="input-datepicker">
                                            <DatePicker
                                                selected={this.state.startDate}
                                                onChange={this.handleChange.bind(this)}
                                                className="input-datepicker__date"
                                                placeholderText="Date"
                                            />
                                            <span className="input-datepicker__status-bar" />
                                        </label>
                                    </div>
                                    <div className="panel-objective__dropdown">
                                        <span className="label">Objective Type</span>
                                        <InputDropdown choices={choices} />
                                    </div>
                                    <div className="panel-objective__private">
                                        <span className="label">Visibility</span>
                                        <InputSwitch value={true} onChange={() => console.log("bonjour")} title="Private" />
                                    </div>
                                </div>
                            </div>
                            <div className="row panel-objective__row">
                                <div className="col-lg-12 col-xs-12">
                                    <span className="label">Progress</span>
                                    <ScaleLine
                                        percentCursor={this.props.objectivesStore!.objectives[0].target}
                                        percentTarget={75}
                                        onMovePointer={value => this.getIndexPointer(value)}
                                        configColor={[
                                            {
                                                percent: 30,
                                                color: "#D11548"
                                            },
                                            {
                                                percent: 50,
                                                color: "#FD9141"
                                            },
                                            {
                                                percent: 100,
                                                color: "#308649"
                                            }
                                        ]}
                                    />
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-xs-12 panel-objective__comment">
                            <Comment />
                        </div>
                    </div>
                </div>
                <footer className="panel-objective-footer">
                    <Button
                        style="secondary"
                        status="danger"
                        size="normal"
                        componentClass="mr-20"
                        onClick={() => this.props.emitterStore!.toggleModalObjective()}
                    >
                        Cancel
                    </Button>
                    <Button
                        style="primary"
                        status="success"
                        size="normal"
                        onClick={() => this.props.emitterStore!.toggleModalObjective()}
                    >
                        Save
                    </Button>
                </footer>
            </div>
        );
    }
}
