import * as React from "react";

export class TSNavbar extends React.Component<{}, {}> {
    public render() {
        return (
            <nav className="navFixed">
                <div className="header-container">
                    <div className="logo">
                        <img src="assets/images/talentsoft-logo.svg" alt="TalentSoft Logo" />
                    </div>
                    <div id="nav" className="navigation">
                        <span className="module-name">ONGOING</span>
                        <span className="icon-bottom" />
                    </div>
                    <div className="search-area">
                        <input type="search" placeholder="Search an employee..." />
                    </div>
                    <div className="notification-area">
                        <span className="icon-notification" />
                    </div>
                    <div className="settings-cta">
                        <span className="icon-setting" />
                    </div>
                    <div className="user-area">
                        <div className="picture-user">
                            <div
                                className="avatar avatar--feed"
                                style={{
                                    backgroundImage:
                                        "url(http://justcuriousshow.com/wp-content/uploads/2017/07/ArtStreiber2-e1501278066158.jpg)"
                                }}
                            />
                        </div>
                        <div className="message-user">
                            <span>Hello Isabelle</span>
                        </div>
                        <div className="logout">
                            <span className="icon-logout" />
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}
