import * as React from "react";
import { CommentPost } from "./comment-post";

export interface CommentState {}

export interface CommentProps {}

export class Comment extends React.Component<CommentProps, {}> {
    constructor(props: CommentProps) {
        super(props);
    }

    render() {
        return (
            <section className="comment">
                <header className="panel-header justify-content-between">
                    <h2>Comments</h2>
                </header>
                <div className="comment__wrapper">
                    <div className="comment">
                        <div className="comment__area">
                            <div className="comment__avatar avatar--collaborator avatar avatar--widget">TS</div>
                            <textarea className="comment__textarea" placeholder="Post your comment here..." />
                        </div>
                        <div className="comment__button">
                            <button
                                role="button"
                                className=" button button-primary button-primary--normal button-primary--default"
                            >
                                Post
                            </button>
                        </div>
                    </div>
                </div>
                <CommentPost
                    comments={[
                        {
                            comment:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elit eros, iaculis in posuere ac, pulvinar ut mauris. Integer suscipit ipsum eu volutpat.",
                            author: "par Stéphanie Gomes",
                            time: "10:30",
                            date: "bonsoir"
                        },
                        {
                            comment:
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elit eros, iaculis in posuere ac, pulvinar ut mauris. Integer suscipit ipsum eu volutpat.",
                            author: "par Francois Petit",
                            time: "9:30",
                            date: "bonsoir"
                        }
                    ]}
                />
            </section>
        );
    }
}
