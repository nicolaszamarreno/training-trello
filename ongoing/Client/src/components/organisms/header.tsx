import * as React from "react";
import { Button } from "../atoms/button";
import { inject, observer } from "mobx-react";
import { EmitterStore } from "../../stores/emitter-store";
import { ObjectivesStore } from "../../stores/objectives-store";

export interface HeaderProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}

@inject("emitterStore")
@observer
export class Header extends React.Component<HeaderProps, {}> {
    constructor(props: HeaderProps) {
        super(props);
    }

    fixedMenu(header: HTMLElement) {
        const $header = header;
        const $appContainer = document.querySelector("#app");

        window.addEventListener("scroll", event => {
            if ($appContainer)
                if ($header.getBoundingClientRect().bottom < $header.getBoundingClientRect().height) {
                    $appContainer.classList.add("header__sticky--isSticky");
                } else if ($header.getBoundingClientRect().bottom > $header.getBoundingClientRect().height) {
                    $appContainer.classList.remove("header__sticky--isSticky");
                }
        });
    }

    render() {
        return (
            <header className="header header--full" ref={header => this.fixedMenu(header!)}>
                <div className="header__sticky">
                    <div className="header__title">
                        <h1 className="color--smart">My Team</h1>
                    </div>
                    <div className="header__action">
                        <Button
                            onClick={() => this.props.emitterStore!.toggleFilter()}
                            size="normal"
                            status="default"
                            style="secondary"
                            badgeValue={10}
                            icon="button__icon icon-show"
                        >
                            Population
                        </Button>
                        <Button
                            onClick={() => this.props.emitterStore!.toggleModalObjective()}
                            size="normal"
                            status="default"
                            style="primary"
                            componentClass="ml-20"
                        >
                            New objective
                        </Button>
                    </div>
                </div>
            </header>
        );
    }
}
