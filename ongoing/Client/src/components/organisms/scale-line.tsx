import * as React from "react";

export interface ConfigColor {
    percent: number;
    color: string;
}

export interface ScaleLineProps {
    percentCursor: number;
    percentTarget: number;
    configColor: ConfigColor[];
    onMovePointer?: (value: number) => void;
}

export interface EventDrag {
    clientX: number;
    clientY: number;
}

export interface ScaleLineState {
    isTransition: boolean;
}

export class ScaleLine extends React.Component<ScaleLineProps, ScaleLineState> {
    private line!: HTMLElement;

    constructor(props: ScaleLineProps) {
        super(props);

        this.state = {
            isTransition: true
        };
    }

    render() {
        const transitionStyle = this.state.isTransition ? "all 0.5s" : "";
        return (
            <div className="scaleLine" ref={line => (this.line = line!)}>
                <div
                    className="scaleLine-tooltip"
                    onMouseDown={this.movePoint.bind(this)}
                    style={{ left: `${this.props.percentCursor}%`, transition: transitionStyle }}
                >
                    <span className="scaleLine-tooltip__number">
                        <i className="icon-location" />
                        {this.props.percentCursor}%
                    </span>
                    <span className="scaleLine-tooltip__pointer scaleLine-tooltip__pointer--percent" />
                </div>
                <span
                    className="scaleLine-tooltip__pointer scaleLine-tooltip__pointer--target"
                    style={{ left: `${this.props.percentTarget}%` }}
                    data-target={`Target ${this.props.percentTarget}%`}
                >
                    <i className="icon-target" />
                </span>
                <div className="scaleLine-progress">
                    <div
                        className="scaleLine-progress__state"
                        style={{
                            width: `${this.props.percentCursor}%`,
                            backgroundColor: this.getColor(),
                            transition: transitionStyle
                        }}
                    />
                </div>
            </div>
        );
    }

    getColor() {
        let colorLine: string;
        if (this.props.percentCursor < this.props.configColor[0].percent) {
            colorLine = this.props.configColor[0].color;
        } else if (this.props.percentCursor < this.props.configColor[1].percent) {
            colorLine = this.props.configColor[1].color;
        } else if (this.props.percentCursor < this.props.configColor[2].percent) {
            colorLine = this.props.configColor[2].color;
        } else {
            colorLine = "#5A9CDE";
        }

        return colorLine;
    }

    movePoint(event: Event & EventDrag) {
        event.preventDefault();
        const widthLine = this.line.offsetWidth;
        const $pointer = event.currentTarget as HTMLElement;

        const onMouseMove = (event: Event & EventDrag) => {
            let newLeft = event.clientX - this.line.getBoundingClientRect().left;
            if (newLeft < 0) {
                newLeft = 0;
            }

            let rightEdge = widthLine - $pointer.offsetWidth;
            if (newLeft > rightEdge) {
                newLeft = rightEdge;
            }

            let value = Math.round(newLeft / widthLine * 100);

            $pointer.style.left = `${value}%`;
            this.props.onMovePointer!(value);
            this.setState({
                isTransition: false
            });
        };

        const onMouseUp = () => {
            document.removeEventListener("mouseup", onMouseUp);
            document.removeEventListener("mousemove", onMouseMove);
            this.setState({
                isTransition: true
            });
        };

        document.addEventListener("mousemove", onMouseMove);
        document.addEventListener("mouseup", onMouseUp);
    }
}
