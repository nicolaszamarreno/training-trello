import * as React from "react";
import { EmitterStore } from "../../stores/emitter-store";
import { inject, observer } from "mobx-react";

export interface FilterProps {
    emitterStore?: EmitterStore;
}

@inject("emitterStore")
@observer
export class Filter extends React.Component<FilterProps, {}> {
    constructor(props: FilterProps) {
        super(props);
    }

    render() {
        return (
            <div className={`filter ${this.props.emitterStore!.FilterIsOpen ? "filter--active" : ""}`}>
                <div className="filter__wrapper">
                    <div className="wrapper--full">
                        <h2 className="filter__title">Select the employee you want to review</h2>
                        <ul className="filter__list">
                            <li className="slider-slide">
                                <div
                                    className="avatar avatar--feed avatar--collaborator card-person__picture"
                                    style={{
                                        backgroundImage:
                                            "url(http://blog.grainedephotographe.com/wp-content/uploads/2014/03/%C2%A9-Danny-Santos-Portraits-of-Strangers-17.jpg)"
                                    }}
                                />
                                <div className="slider-slide__desc card-person">
                                    <strong className="card-person__firstname">Joe Doe</strong>
                                    <p className="card-person__lastname">Feedback</p>
                                    <p className="card-person__job">Feedback</p>
                                </div>
                                <i className="card-person__icon icon-show" />
                            </li>
                            <li className="slider-slide">
                                <div
                                    className="avatar avatar--feed avatar--collaborator card-person__picture"
                                    style={{
                                        backgroundImage:
                                            "url(http://blog.grainedephotographe.com/wp-content/uploads/2014/03/%C2%A9-Danny-Santos-Portraits-of-Strangers-17.jpg)"
                                    }}
                                />
                                <div className="slider-slide__desc card-person">
                                    <strong className="card-person__firstname">Joe Doe</strong>
                                    <p className="card-person__lastname">Feedback</p>
                                    <p className="card-person__job">Feedback</p>
                                </div>
                                <i className="card-person__icon icon-show" />
                            </li>
                            <li className="slider-slide">
                                <div
                                    className="avatar avatar--feed avatar--collaborator card-person__picture"
                                    style={{
                                        backgroundImage:
                                            "url(http://blog.grainedephotographe.com/wp-content/uploads/2014/03/%C2%A9-Danny-Santos-Portraits-of-Strangers-17.jpg)"
                                    }}
                                />
                                <div className="slider-slide__desc card-person">
                                    <strong className="card-person__firstname">Joe Doe</strong>
                                    <p className="card-person__lastname">Feedback</p>
                                    <p className="card-person__job">Feedback</p>
                                </div>
                                <i className="card-person__icon icon-show" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
