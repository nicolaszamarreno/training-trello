import * as React from "react";
import DatePicker from "react-datepicker";
import { Moment } from "moment";
import { InputLabel } from "../molecules/input-label";
import { TextareaLabel } from "../molecules/textarea-label";
import { Button } from "../atoms/button";
import { FormGroup } from "./form-group";
import { InputDropdown } from "../molecules/input-dropdown";
import { Popin } from "./popin";
const moment = require("moment");

export interface PopinAddObjectiveProps {
    closePopin: () => void;
}

export interface PopinAddObjectiveState {
    fieldValid: boolean;
    startDate: Moment;
    objectiveTitle: string;
    objectiveDescription: string;
    objectiveType: string;
    targetPercent: number;
}

class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps) {
        super(props);

        this.state = {
            fieldValid: true,
            startDate: moment(),
            objectiveTitle: "",
            objectiveDescription: "",
            objectiveType: "",
            targetPercent: 0
        };
    }

    targetPercent(event: React.FormEvent<HTMLInputElement>) {
        let value: number = parseInt(event.currentTarget.value);

        if (!isNaN(value) && value > 0 && value <= 100) {
            this.setState({
                targetPercent: value,
                fieldValid: true
            });
        } else {
            this.setState({
                fieldValid: false
            });
        }
    }

    objectiveDescription(event: React.FormEvent<HTMLInputElement>) {
        let description: string = event.currentTarget.value;

        this.setState({
            objectiveDescription: description
        });
    }

    objectiveTitle(event: React.FormEvent<HTMLInputElement>) {
        let title: string = event.currentTarget.value;

        this.setState({
            objectiveTitle: title
        });
    }

    handleSubmit(event: React.FormEvent<HTMLInputElement>) {
        event.preventDefault();

        this.props.closePopin();
    }

    handleChange(date: Moment) {
        this.setState({
            startDate: date
        });
    }

    render() {
        const choices = [
            {
                label: "Personnal",
                active: false
            },
            {
                label: "Collective",
                active: true
            },
            {
                label: "Shared",
                active: false
            }
        ];

        return (
            <Popin title="Create an objectif">
                <FormGroup onSubmit={this.handleSubmit.bind(this)}>
                    <InputLabel
                        idControl="objectiveTitle"
                        inputType="text"
                        componentClass="mb-20"
                        onChange={this.objectiveTitle.bind(this)}
                        inputValid={this.state.fieldValid}
                        warningMessage="The number must be between 0 and 100%"
                    >
                        Objective title
                    </InputLabel>

                    <div className="d-flex mb-20">
                        <div className="w-50 mr-20">
                            <span className="label">Objective type</span>
                            <InputDropdown choices={choices} />
                        </div>

                        <div className="w-50">
                            <span className="label">End Date</span>
                            <label className="input-datepicker">
                                <DatePicker
                                    selected={this.state.startDate}
                                    onChange={this.handleChange.bind(this)}
                                    className="input-datepicker__date"
                                    placeholderText="I have been cleared!"
                                />
                                <span className="input-datepicker__status-bar" />
                            </label>
                        </div>
                    </div>

                    <TextareaLabel idControl="note" classComponent="mb-20" onChange={this.objectiveDescription.bind(this)}>
                        Description
                    </TextareaLabel>

                    <InputLabel
                        idControl="target"
                        inputType="number"
                        inputPlaceholder="...%"
                        componentClass="mb-20"
                        onChange={this.targetPercent.bind(this)}
                        inputValid={this.state.fieldValid}
                        warningMessage="The number must be between 0 and 100%"
                    >
                        Target
                    </InputLabel>

                    <div className="popin__callAction d-flex justify-content-end">
                        <Button
                            style="secondary"
                            size="normal"
                            status="danger"
                            componentClass="popin__cancel mr-20"
                            onClick={this.props.closePopin}
                        >
                            Cancel
                        </Button>
                        <Button role="submit" size="normal" status="success" style="primary">
                            Yes, apply
                        </Button>
                    </div>
                </FormGroup>
            </Popin>
        );
    }
}

export { PopinAddObjective };
