import * as React from "react";
import { InputLabel } from "../molecules/input-label";

export interface ObjectiveTodoListState {
    inputValue: string;
}

export interface ObjectiveTodoListProps {
    classComponent?: string;
    tags: string[];
    onClickTask?: (item: string) => void;
}

export class AddTag extends React.Component<ObjectiveTodoListProps, ObjectiveTodoListState> {
    constructor(props: ObjectiveTodoListProps) {
        super(props);

        this.state = {
            inputValue: ""
        };
    }

    render() {
        return (
            <div className="tagEnter">
                <InputLabel
                    inputType="text"
                    inputValue={this.state.inputValue}
                    inputPlaceholder="Add your tag"
                    icon="icon-tag"
                    iconClass="color--smart"
                    onChange={this.handleChange.bind(this)}
                    onKeyPress={this.handleKeyPress.bind(this)}
                    inputValid={true}
                />
                <ul className="tagEnter__list">{this.renderList()}</ul>
            </div>
        );
    }

    handleKeyPress(event: KeyboardEvent) {
        if (event.key === "Enter") {
            this.setState({
                inputValue: ""
            });
        }
    }

    handleChange(event: React.FormEvent<HTMLInputElement>) {
        let tagName: string = event.currentTarget.value;

        this.setState({
            inputValue: tagName
        });
    }

    renderList() {
        return this.props.tags.map((tag, index) => {
            return (
                <li key={index} className="tagEnter__tag">
                    <i className="tagEnter__icon icon-delete" />
                    {tag}
                </li>
            );
        });
    }
}
