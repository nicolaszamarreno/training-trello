import * as React from "react";
import Column from "../organisms/column";
import { observer, inject } from "mobx-react";
import { ObjectivesStore } from "../../stores/objectives-store";

export interface BoardProps {
    objectivesStore?: ObjectivesStore;
}

@inject("objectivesStore")
@observer
export class Board extends React.Component<BoardProps, {}> {
    constructor(props: BoardProps) {
        super(props);
    }

    public render() {
        return (
            <div className="board">
                {this.props.objectivesStore!.objectives.map((objective, index) => {
                    return (
                        <Column
                            key={index}
                            idObjective={objective.id}
                            titleObjective={objective.title}
                            indexObjective={index}
                            isPrivate={objective.isPrivate}
                            keyResults={objective.keyResults}
                            colorObjective={objective.colorObjective}
                        />
                    );
                })}
            </div>
        );
    }
}
