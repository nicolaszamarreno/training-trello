import * as React from "react";
import { MenuItem, TooltipMenu } from "./tooltip-menu";
import { Button, ButtonProps } from "../atoms/button";

export interface ButtonMenuProps extends ButtonProps {
    actionsList: MenuItem[];
}

export interface MenuItemIconsProps {
    iconClass: string;
    color?: string;
    label?: string;
    onClick?: () => any;
}

export interface ButtonMenuState {
    isOpen: boolean;
}

export class ButtonMenu extends React.Component<ButtonMenuProps, ButtonMenuState> {
    constructor(props: ButtonMenuProps) {
        super(props);

        this.state = {
            isOpen: false
        };
    }

    public componentDidMount() {
        const hideMenu = () => {
            this.setState({
                isOpen: false
            });
        };

        window.addEventListener("scroll", hideMenu);

        window.addEventListener("resize", hideMenu);
    }

    public render() {
        return (
            <Button
                label={this.props.label}
                size={this.props.size}
                status={this.props.status}
                style={this.props.style}
                role={this.props.role}
                color={this.props.color}
                backgroundColor={this.props.backgroundColor}
                borderColor={this.props.borderColor}
                icon={this.props.icon}
                componentClass={this.props.componentClass}
                onClick={this.handleClick.bind(this)}
            >
                <TooltipMenu actionsList={this.props.actionsList} isOpen={this.state.isOpen} />
            </Button>
        );
    }

    public handleClick() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
}
