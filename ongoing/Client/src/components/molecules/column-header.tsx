import * as React from "react";
import { ButtonMenu } from "./button-menu";

export interface ColumnHeaderProps {
    titleObjective: string;
    indexObjective: number;
    colorObjective: string;
    deadline?: string;
    comments?: string;
    isPrivate: boolean;
}

export class ColumnHeader extends React.Component<ColumnHeaderProps, {}> {
    constructor(props: ColumnHeaderProps) {
        super(props);
    }

    public render() {
        return (
            <header className="column-header">
                <span className="column-header__category" style={{ backgroundColor: this.props.colorObjective }} />
                <div className="column-header__wrapper">
                    <div className="column-header__title">
                        <h2 className="column-header__name">{this.props.titleObjective}</h2>
                    </div>
                    <ButtonMenu
                        style="round"
                        icon="icon-more"
                        componentClass="column-header__menu"
                        actionsList={[
                            {
                                color: "color--smart",
                                iconClass: "icon-edit",
                                label: "Edit"
                            },
                            {
                                color: "color--smart",
                                iconClass: "icon-files",
                                label: "Copy"
                            },
                            {
                                color: "color--smart",
                                iconClass: "icon-check",
                                label: "Publish"
                            },
                            {
                                color: "color--smart",
                                iconClass: "icon-discussion",
                                label: "Comment"
                            },
                            {
                                color: "color--smart",
                                iconClass: "icon-book",
                                label: "Archive"
                            },
                            {
                                color: "color--smart",
                                iconClass: "icon-delete",
                                label: "Close"
                            },
                            {
                                color: "color--ruban",
                                iconClass: "icon-trash",
                                label: "Delete"
                            }
                        ]}
                    />
                    <ul className="card-indicators">
                        {this.props.deadline && (
                            <li>
                                <i className="icon-time card-indicators__icon" /> {this.props.deadline}
                            </li>
                        )}
                        {this.props.comments && (
                            <li>
                                <i className="icon-discussion2 card-indicators__icon" /> {this.props.comments}
                            </li>
                        )}
                        <li>{this.props.isPrivate ? "Private" : "Public"}</li>
                    </ul>
                </div>
            </header>
        );
    }
}
