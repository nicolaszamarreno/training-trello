import * as React from "react";

export interface Choice {
    label: string;
    icon?: string;
    isActive?: boolean;
}

export interface InputDropdownProps {
    choices: Choice[];
    label?: string;
}

export interface InputDropdownState {
    displayList: boolean;
    selectedItem?: Choice;
}

export class InputDropdown extends React.Component<InputDropdownProps, InputDropdownState> {
    constructor(props: InputDropdownProps) {
        super(props);

        this.state = {
            displayList: false
        };
    }

    componentWillMount() {
        const currentState = this.props.choices.filter(item => item.isActive === true);
        if (currentState.length > 0) {
            this.setState({
                selectedItem: currentState[0]
            });
        }
    }

    handleClickOpenList() {
        this.setState({
            displayList: !this.state.displayList
        });
    }

    handleClickSelectionItem(selectedChoice: Choice) {
        this.setState({
            selectedItem: selectedChoice,
            displayList: !this.state.displayList
        });
    }

    render() {
        return (
            <div className="input-select">
                <div className={`input-select__wrapper ${this.state.displayList ? "input-select--open" : ""}`}>
                    <div className="input-select__selection">
                        {this.state.selectedItem && (
                            <i className={`input-select__icon color--smart ${this.state.selectedItem.icon}`} />
                        )}
                        <span className="input-select__label">
                            {this.state.selectedItem ? this.state.selectedItem.label : this.props.label}
                        </span>
                    </div>
                    <button type="button" className="input-select__button" onClick={this.handleClickOpenList.bind(this)}>
                        <i className="input-select-dropdown__down icon-bottom" />
                        <i className="input-select-dropdown__up icon-top" />
                    </button>
                    <span className="input-select__status-bar" />
                </div>
                <ul className="input-select__list">
                    {this.props.choices.map((item, index) => {
                        return (
                            <li key={index} className="input-select__item" onClick={() => this.handleClickSelectionItem(item)}>
                                {item.icon && <i className={`input-select__icon color--smart ${item.icon}`} />}
                                {item.label}
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}
