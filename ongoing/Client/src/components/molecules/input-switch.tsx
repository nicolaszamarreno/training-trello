import * as React from "react";
import { ChangeEvent } from "react";

export interface InputSwitchProps {
    type?: "checkbox" | "radio";
    value: boolean;
    title?: string;
    name?: string;
    label?: string;
    onChange: (value: boolean) => void;
}

export class InputSwitch extends React.Component<InputSwitchProps, {}> {
    changeCheckbox(event: ChangeEvent<HTMLInputElement>) {
        this.props.onChange(event.target.checked);
    }

    public render() {
        return (
            <label className="input__choice">
                {this.props.label && <span className="input__choice__label">{this.props.label}</span>}
                <input
                    className="input-switch__input"
                    type={this.props.type || "checkbox"}
                    name={this.props.name}
                    checked={this.props.value}
                    onChange={this.changeCheckbox}
                />
                <span className="input-switch__apparence" />
                {this.props.title && <span className="input__value text--regular">{this.props.title}</span>}
            </label>
        );
    }
}
