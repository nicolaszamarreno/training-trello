import * as React from "react";
import { DragSource, DropTarget } from "react-dnd";
import { ItemTypes } from "../../constants";
import { findDOMNode } from "react-dom";
import { observer, inject } from "mobx-react";
import { KeyResult } from "../../stores/objectives-store";

export interface CardProps {
    id: number;
    index: number;
    indexObjective: number;
    keyResult: KeyResult;
    tag: string;
    tagColor: string;
    name: string;
    comments: string;
    deadline: string;
    onClick: (keyResult: KeyResult) => void;
}

export interface CardState {
    toggleModalCard: boolean;
}

export interface CardDNDProps extends CardProps {
    connectDragSource?: Function;
    connectDropTarget?: Function;
    connectDragPreview?: Function;
    isDragging?: boolean;
    change?: boolean;
    moveCard: (dragIndex: number, hoverIndex: number, idObjective: number) => void;
}

const cardSource = {
    beginDrag(props: CardDNDProps) {
        return {
            id: props.id,
            index: props.index,
            idObjective: props.indexObjective
        };
    }
};

const cardTarget = {
    hover(props: CardDNDProps, monitor: any, component: any) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;
        const indexObjective = props.indexObjective;

        if (dragIndex === hoverIndex) {
            return;
        }

        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        const clientOffset = monitor.getClientOffset();

        const hoverClientY = clientOffset.y - hoverBoundingRect.top;

        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return;
        }

        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return;
        }

        if (monitor.getItem().idObjective !== props.indexObjective) {
            return;
        }
        props.moveCard(dragIndex, hoverIndex, indexObjective);
        monitor.getItem().index = hoverIndex;
    }
};

@DropTarget(ItemTypes.CARD, cardTarget, connect => ({
    connectDropTarget: connect.dropTarget()
}))
@DragSource(ItemTypes.CARD, cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
}))
@inject("emitterStore")
@observer
export class Card extends React.Component<CardDNDProps, {}> {
    constructor(props: CardDNDProps) {
        super(props);
    }
    render() {
        const { isDragging, connectDragSource, connectDropTarget } = this.props;
        const styles = {
            border: isDragging ? ".1rem dashed #D1D3D5" : "",
            opacity: isDragging ? 0.3 : 1
        };

        return connectDragSource!(
            connectDropTarget!(
                <div className="card" style={styles} onClick={() => this.props.onClick(this.props.keyResult)}>
                    <p className="card__title">{this.props.keyResult.title}</p>
                    <ul className="card-indicators">
                        {this.props.keyResult.tag && (
                            <li>
                                <span className="tag card-indicators__tag">{this.props.keyResult.tag}</span>
                            </li>
                        )}
                        {this.props.keyResult.deadline && (
                            <li>
                                <i className="icon-time card-indicators__icon" /> {this.props.keyResult.deadline}
                            </li>
                        )}
                        {this.props.keyResult.comments && (
                            <li className="card-indicators__item">
                                <i className="icon-discussion2 card-indicators__icon" /> {this.props.keyResult.comments}
                            </li>
                        )}
                        {this.props.keyResult.comments && (
                            <li>
                                <span
                                    className="avatar avatar--feed card-indicators__collab"
                                    style={{
                                        backgroundImage:
                                            "url(http://blog.grainedephotographe.com/wp-content/uploads/2014/03/%C2%A9-Danny-Santos-Portraits-of-Strangers-17.jpg)"
                                    }}
                                />
                            </li>
                        )}
                    </ul>
                </div>
            )
        );
    }
}
