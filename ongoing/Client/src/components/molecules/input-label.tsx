import * as React from "react";
import { Label } from "../atoms/label";
import { Input } from "../atoms/input";

export type InputTypes = "text" | "number" | "mail";

export interface InputLabelProps {
    componentClass?: string;
    pattern?: string;
    inline?: boolean;
    idControl?: string;
    inputType: InputTypes;
    inputComponentClass?: string;
    inputPlaceholder?: string;
    inputValue?: string;
    onChange?: () => void;
    onKeyPress?: () => void;
    inputValid?: boolean;
    warningMessage?: string;
    icon?: string;
    iconClass?: string;
}

class InputLabel extends React.Component<InputLabelProps, {}> {
    constructor(props: InputLabelProps) {
        super(props);
    }

    render() {
        return (
            <div
                className={`d-flex ${this.props.componentClass ? this.props.componentClass : ""} ${
                    this.props.inline ? "flex-row" : "flex-column"
                }`}
            >
                <Label idControl={this.props.idControl}>{this.props.children}</Label>
                <Input
                    type={this.props.inputType}
                    idControl={this.props.idControl}
                    placeholder={this.props.inputPlaceholder}
                    componentClass={this.props.inputComponentClass}
                    value={this.props.inputValue}
                    pattern={this.props.pattern}
                    onChange={this.props.onChange}
                    inputValid={this.props.inputValid}
                    warningMessage={this.props.warningMessage}
                    icon={this.props.icon}
                    iconClass={this.props.iconClass}
                    onKeyPress={this.props.onKeyPress}
                />
            </div>
        );
    }
}

export { InputLabel };
