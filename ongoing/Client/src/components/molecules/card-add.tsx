import * as React from "react";
import { Button } from "../atoms/button";
import { inject, observer } from "mobx-react";
import { ObjectivesStore } from "../../stores/objectives-store";

export interface CardAddState {
    valueNewKeyResult: string;
}

export interface CardAddStore {
    objectivesStore?: ObjectivesStore;
    indexObjective: number;
    idObjective: number;
    onCancel: () => void;
}

@inject("objectivesStore")
@observer
export class CardAdd extends React.Component<CardAddStore, CardAddState> {
    private inputAdd: HTMLElement | null;

    constructor(props: CardAddStore) {
        super(props);

        this.state = {
            valueNewKeyResult: ""
        };

        this.inputAdd = null;
    }

    onChange(event: React.FormEvent<HTMLTextAreaElement>) {
        let value = event.currentTarget.value;

        this.setState({
            valueNewKeyResult: value
        });
    }

    componentDidMount() {
        this.inputAdd!.focus();
    }

    handleCardAddOnKeyDown(keyCap: React.KeyboardEvent<EventTarget>) {
        const ENTER_KEY = 13;
        const ESCAPE_KEY = 27;

        if (keyCap.keyCode === ENTER_KEY) {
            this.saveKeyResult();
            this.props.onCancel();
        } else if (keyCap.keyCode === ESCAPE_KEY) {
            this.props.onCancel();
        }
    }

    saveKeyResult() {
        if (this.state.valueNewKeyResult !== "") {
            this.props.objectivesStore!.addKeyResult(this.props.idObjective, this.state.valueNewKeyResult);
            this.props.onCancel();
            this.setState({
                valueNewKeyResult: ""
            });
        }
    }

    render() {
        return (
            <div className="card__add">
                <div className="card__overlaid" />
                <div className="card card--add">
                    <textarea
                        className="card__textarea"
                        onChange={value => this.onChange(value)}
                        onKeyDown={keycap => this.handleCardAddOnKeyDown(keycap)}
                        ref={input => (this.inputAdd = input)}
                        value={this.state.valueNewKeyResult}
                    />
                    <Button
                        style="secondary"
                        size="normal"
                        status="danger"
                        componentClass="popin__cancel mr-20"
                        onClick={() => this.props.onCancel()}
                    >
                        Cancel
                    </Button>
                    <Button size="normal" status="success" style="primary" onClick={() => this.saveKeyResult()}>
                        Save
                    </Button>
                </div>
            </div>
        );
    }
}
