import * as React from "react";

export interface MenuItem {
    iconClass?: string;
    color?: string;
    label?: string;
    onClick?: () => any;
}

export interface TooltipMenuProps {
    actionsList: MenuItem[];
    isOpen: boolean;
}

export class TooltipMenu extends React.Component<TooltipMenuProps, {}> {
    private menuList?: HTMLElement | null;

    constructor(props: TooltipMenuProps) {
        super(props);
    }

    public render() {
        return (
            <nav
                className={`panel-menu ${this.props.isOpen ? "panel-menu--open" : ""}`}
                style={{ zIndex: 1500 }}
                ref={menuList => (this.menuList = menuList!)}
            >
                <span className="panel-menu__marker" />
                {this.renderList()}
            </nav>
        );
    }

    public renderList() {
        const elementWithoutIcon: JSX.Element[] = [];
        const elementWithIcon: JSX.Element[] = [];

        if (this.props.actionsList && this.props.actionsList.length > 0) {
            this.props.actionsList.forEach((button, index) => {
                if (button.iconClass) {
                    elementWithoutIcon.push(
                        <li
                            className={`panel-menu__item ${button.color ? button.color : ""}`}
                            key={index}
                            title={button.label}
                            onClick={button.onClick}
                        >
                            <i className={`panel-menu__icon ${button.iconClass}`} />
                            <a>{button.label}</a>
                        </li>
                    );
                } else {
                    elementWithIcon.push(
                        <li className="panel-menu__item" key={index} title={button.label} onClick={button.onClick}>
                            <a>{button.label}</a>
                        </li>
                    );
                }
            });
        }

        return (
            <div>
                {elementWithoutIcon.length > 0 && <ul className="panel-menu__list">{elementWithoutIcon}</ul>}

                {elementWithIcon.length > 0 && (
                    <div className="panel-menu__list panel-menu__list--scroll">
                        <ul className="panel-menu__wrapper scrollbar--active ps ps--active-y">{elementWithIcon}</ul>
                    </div>
                )}
            </div>
        );
    }

    componentDidMount() {
        this.animElementList();
    }

    public animElementList() {
        const $items = this.menuList!.querySelectorAll("li");

        Array.prototype.forEach.call($items, ($el: Element, index: number) => {
            const $item = $el as HTMLElement;
            $item.style.transition = `all 0.2s ${index * 0.1}s`;
        });
    }
}
