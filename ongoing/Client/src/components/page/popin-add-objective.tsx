import * as React from "react";
import { InputLabel } from "../molecules/input-label";
import { Button } from "../atoms/button";
import { FormGroup } from "../organisms/form-group";
import { Popin } from "../organisms/popin";
import { TextareaLabel } from "../molecules/textarea-label";
import { ObjectivesStore } from "../../stores/objectives-store";
import { inject, observer } from "mobx-react";
import { InputDropdown } from "../molecules/input-dropdown";
import { InputSwitch } from "../molecules/input-switch";
import { EmitterStore } from "../../stores/emitter-store";

export interface PopinAddObjectiveProps {
    closePopin: () => void;
    objectiveId: number;
    objectivesStore?: ObjectivesStore;
    emitterStore?: EmitterStore;
}

export interface PopinAddObjectiveState {
    fieldValid: boolean;
    objectiveTitle: string;
    objectiveDescription: string;
    objectiveType: string;
    targetPercent: number;
    isPrivate: boolean;
}

@inject("objectivesStore", "emitterStore")
@observer
export class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps) {
        super(props);

        this.state = {
            fieldValid: true,
            objectiveTitle: "",
            objectiveDescription: "",
            objectiveType: "",
            targetPercent: 0,
            isPrivate: true
        };
    }

    targetPercent(event: React.FormEvent<HTMLInputElement>) {
        let value: number = parseInt(event.currentTarget.value);

        if (!isNaN(value) && value > 0 && value <= 100) {
            this.setState({
                targetPercent: value,
                fieldValid: true
            });
        } else {
            this.setState({
                fieldValid: false
            });
        }
    }

    objectiveDescription(event: React.FormEvent<HTMLInputElement>) {
        let description: string = event.currentTarget.value;

        this.setState({
            objectiveDescription: description
        });
    }

    objectiveTitle(event: React.FormEvent<HTMLInputElement>) {
        let title: string = event.currentTarget.value;

        this.setState({
            objectiveTitle: title
        });
    }

    handleSubmit(event: React.FormEvent<HTMLInputElement>) {
        event.preventDefault();
        this.props.objectivesStore!.addKeyResult(this.props.objectiveId, this.state.objectiveTitle);
        this.props.closePopin();
    }

    changePrivate(isPrivate: boolean) {
        this.setState({
            isPrivate: !this.state.isPrivate
        });
    }

    render() {
        const choices = [
            {
                label: "Achieved",
                icon: "icon-trophy",
                isActive: false
            },
            {
                label: "In Progress",
                icon: "icon-indicator",
                isActive: true
            },
            {
                label: "New",
                icon: "icon-jalon",
                isActive: false
            }
        ];

        return (
            <Popin title="Create an objective">
                <FormGroup onSubmit={this.handleSubmit.bind(this)}>
                    <InputLabel
                        idControl="objectiveTitle"
                        inputType="text"
                        componentClass="mb-20"
                        onChange={this.objectiveTitle.bind(this)}
                        inputValid={this.state.fieldValid}
                        warningMessage="The number must be between 0 and 100%"
                    >
                        Objective title
                    </InputLabel>

                    <TextareaLabel idControl="note" classComponent="mb-20" onChange={this.objectiveDescription.bind(this)}>
                        Description
                    </TextareaLabel>

                    <div className="row">
                        <div className="col-2">
                            <InputLabel
                                idControl="target"
                                inputType="number"
                                inputPlaceholder="...%"
                                componentClass="mb-20"
                                onChange={this.targetPercent.bind(this)}
                                inputValid={this.state.fieldValid}
                                warningMessage="The number must be between 0 and 100%"
                            >
                                Target
                            </InputLabel>
                        </div>
                        <div className="col-2">
                            <InputLabel
                                idControl="target"
                                inputType="number"
                                inputPlaceholder="...%"
                                componentClass="mb-20"
                                onChange={this.targetPercent.bind(this)}
                                inputValid={this.state.fieldValid}
                                warningMessage="The number must be between 0 and 100%"
                            >
                                Weight
                            </InputLabel>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-2">
                            <label className="label">Objectives Types</label>
                            <InputDropdown choices={choices} />
                        </div>
                        <div className="col-2">
                            <InputSwitch
                                label="Private"
                                value={this.state.isPrivate}
                                onChange={isPrivate => this.changePrivate(isPrivate)}
                            />
                        </div>
                    </div>

                    <div className="popin__callAction d-flex justify-content-end">
                        <Button
                            style="secondary"
                            size="normal"
                            status="danger"
                            componentClass="popin__cancel mr-20"
                            onClick={() => this.props.closePopin()}
                        >
                            Cancel
                        </Button>
                        <Button role="submit" size="normal" status="success" style="primary">
                            Yes, apply
                        </Button>
                    </div>
                </FormGroup>
            </Popin>
        );
    }
}
