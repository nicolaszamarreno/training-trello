import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "mobx-react";
import { App } from "./app";
import { ObjectivesStore } from "./stores/objectives-store";
import { EmitterStore } from "./stores/emitter-store";

const objectivesStore = new ObjectivesStore();
const emitterStore = new EmitterStore();

const stores = {
    objectivesStore: objectivesStore,
    emitterStore: emitterStore
};

ReactDOM.render(
    <Provider {...stores}>
        <App />
    </Provider>,
    document.getElementById("app")
);
