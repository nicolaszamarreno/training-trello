export interface OptionsSticky {
    className: string;
    elementAddClass: HTMLElement;
}

export class Sticky {
    private elementTarget: HTMLElement;
    private className: string;
    private elementAddClass: HTMLElement;
    private isStuck: boolean;
    private stickPoint: number;

    constructor(elementIsLooked: HTMLElement, options: OptionsSticky) {
        this.elementTarget = elementIsLooked;
        this.className = options.className;
        this.elementAddClass = options.elementAddClass;

        this.isStuck = false;
        this.stickPoint = this.getDistance(this.elementTarget);

        this.handleScroll();
    }

    /**
     * Return distance
     * @param $element {Node}
     * @returns {number}
     */
    getDistance($element: HTMLElement) {
        return $element.offsetTop;
    }

    /**
     * Init Scroll Event
     */
    handleScroll() {
        window.addEventListener("scroll", event => {
            const distance = this.getDistance(this.elementTarget) - window.pageYOffset;
            const offset = window.pageYOffset;
            const $element = this.elementAddClass ? this.elementAddClass : this.elementTarget;

            if (distance <= 0 && !this.isStuck) {
                $element.classList.add(this.className);
                this.isStuck = true;
            } else if (this.isStuck && offset <= this.stickPoint) {
                $element.classList.remove(this.className);
                this.isStuck = false;
            }
        });
    }
}
