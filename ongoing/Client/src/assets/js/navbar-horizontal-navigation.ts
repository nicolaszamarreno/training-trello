export interface Event {
    currentTarget: HTMLElement;
}

export class NavbarHorizontalClass {
    private $status: HTMLElement;
    private $itemsNavbarHorizontal: NodeList;

    constructor() {
        this.$status = document.querySelector(".navbar-status--horizontal") as HTMLElement;
        this.$itemsNavbarHorizontal = document.querySelectorAll(".navbar--horizontal li") as NodeListOf<HTMLElement>;

        this.$status.style.transition = "all .5s";

        this.initEvent();
    }

    initEvent() {
        const nodelistToArray = Array.prototype.slice.call(this.$itemsNavbarHorizontal);
        for (const $element of nodelistToArray) {
            $element.addEventListener("mouseenter", (event: Event) => {
                const position = event.currentTarget.offsetLeft;
                this.$status.style.transform = `translate3d(${position}px, 0, 0)`;
                this.$status.style.width = `${event.currentTarget.offsetWidth}px`;
            });

            $element.addEventListener("mouseleave", (event: Event) => {
                const selectedItem = document.querySelector(".navbar--horizontal--active") as HTMLElement;
                const position = selectedItem.offsetLeft;
                this.$status.style.transform = `translate3d(${position}px, 0, 0)`;
                this.$status.style.width = `${selectedItem.offsetWidth}px`;
            });
        }
    }
}
