# TrelloLike by Talentsoft

_This tool permit you to assign objectives and missions at your collaborators on a board like Trello_

## Utils

### Install dependencies

_You should install dependancies with Yarn or NPM_

```bash
$ yarn install
```

### Webpack for the development

_Your can use webpack for your development_  
**If you meet issues when you install package, see the [warning](#warning) section**

```bash
# Launch server and connect you on the http://localhost:3000
$ yarn run dev

# Compile and set all files on the `dist` folder
$ yarn run prod
```

## Warning

### Hylia Package

When you will install the packages, **if you are not on the Talentsoft Network** you will cannot install Hylia Package
