﻿using Microsoft.EntityFrameworkCore;
using ongoing.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Config
{
    public class BddContext : DbContext
    {
        public BddContext(DbContextOptions<BddContext> options) : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=objectives.db");
        }

        public DbSet<Objective> Objectives { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<KeyResult> KeyResults { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
