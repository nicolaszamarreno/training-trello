﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Entites
{
    public class Objective
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long Target { get; set; }
        public long CurrentTarget { get; set; }
        public bool IsPrivate { get; set; }
        public string ColorObjective { get; set; }
        public DateTime Deadline { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual List<KeyResult> KeyResults { get; set; }
    }
}
