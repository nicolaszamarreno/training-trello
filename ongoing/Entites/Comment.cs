﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Entites
{
    public class Comment
    {
        public long Id { get; set; }
        public string Post { get; set; }
        public DateTime Date { get; set; }
    }
}
