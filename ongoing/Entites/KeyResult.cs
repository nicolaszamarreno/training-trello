﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Entites
{
    public class KeyResult
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public virtual Objective Objective { get; set; }
    }
}
