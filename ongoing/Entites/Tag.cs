﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Entites
{
    public class Tag
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
}
