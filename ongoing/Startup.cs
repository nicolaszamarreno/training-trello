﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ongoing.Config;
using ongoing.Repositories.Interface;
using ongoing.Repositories.Repository;
using ongoing.Services.Interface;
using ongoing.Services.Service;

namespace ongoing
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddScoped<IObjectiveRepository, ObjectiveRepository>();
            services.AddScoped<IKeyResultRepository, KeyResultRepository>();
            services.AddScoped<IKeyResultService, KeyResultService>();
            services.AddScoped<ITagRepository, TagRepository>();

            services.AddDbContext<BddContext>(options =>
            options.UseSqlite("Data Source=objectives.db"));

            services.AddMvc().AddJsonOptions(
                    options => options.SerializerSettings.ReferenceLoopHandling
                        = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
