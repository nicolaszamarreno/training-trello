export interface OptionsSticky {
    className: string;
    elementAddClass: HTMLElement;
}
export declare class Sticky {
    private elementTarget;
    private className;
    private elementAddClass;
    private isStuck;
    private stickPoint;
    constructor(elementIsLooked: HTMLElement, options: OptionsSticky);
    /**
     * Return distance
     * @param $element {Node}
     * @returns {number}
     */
    getDistance($element: HTMLElement): number;
    /**
     * Init Scroll Event
     */
    handleScroll(): void;
}
