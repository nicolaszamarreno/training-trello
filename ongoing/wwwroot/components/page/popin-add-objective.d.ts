/// <reference types="react" />
import * as React from "react";
import { ObjectivesStore } from "../../stores/objectives-store";
import { EmitterStore } from "../../stores/emitter-store";
export interface PopinAddObjectiveProps {
    closePopin: () => void;
    objectiveId: number;
    objectivesStore?: ObjectivesStore;
    emitterStore?: EmitterStore;
}
export interface PopinAddObjectiveState {
    fieldValid: boolean;
    objectiveTitle: string;
    objectiveDescription: string;
    objectiveType: string;
    targetPercent: number;
    isPrivate: boolean;
}
export declare class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps);
    targetPercent(event: React.FormEvent<HTMLInputElement>): void;
    objectiveDescription(event: React.FormEvent<HTMLInputElement>): void;
    objectiveTitle(event: React.FormEvent<HTMLInputElement>): void;
    handleSubmit(event: React.FormEvent<HTMLInputElement>): void;
    changePrivate(isPrivate: boolean): void;
    render(): JSX.Element;
}
