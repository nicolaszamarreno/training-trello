/// <reference types="react" />
import * as React from "react";
export interface ColumnHeaderProps {
    titleObjective: string;
    indexObjective: number;
    colorObjective: string;
    deadline?: string;
    comments?: string;
    isPrivate: boolean;
}
export declare class ColumnHeader extends React.Component<ColumnHeaderProps, {}> {
    constructor(props: ColumnHeaderProps);
    render(): JSX.Element;
}
