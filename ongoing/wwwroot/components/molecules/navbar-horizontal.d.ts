/// <reference types="react" />
import * as React from "react";
export interface NavbarHorizontalProps {
    menuElements?: object[];
}
export declare class NavbarHorizontal extends React.Component<NavbarHorizontalProps, {}> {
    constructor(props: NavbarHorizontalProps);
    componentDidMount(): void;
    render(): JSX.Element;
}
