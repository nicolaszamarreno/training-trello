/// <reference types="react" />
import * as React from "react";
export declare type InputTypes = "text" | "number" | "mail";
export interface InputLabelProps {
    componentClass?: string;
    pattern?: string;
    inline?: boolean;
    idControl?: string;
    inputType: InputTypes;
    inputComponentClass?: string;
    inputPlaceholder?: string;
    inputValue?: string;
    onChange?: () => void;
    onKeyPress?: () => void;
    inputValid?: boolean;
    warningMessage?: string;
    icon?: string;
    iconClass?: string;
}
declare class InputLabel extends React.Component<InputLabelProps, {}> {
    constructor(props: InputLabelProps);
    render(): JSX.Element;
}
export { InputLabel };
