/// <reference types="react" />
import * as React from "react";
export interface Choice {
    label: string;
    icon?: string;
    isActive?: boolean;
}
export interface InputDropdownProps {
    choices: Choice[];
    label?: string;
}
export interface InputDropdownState {
    displayList: boolean;
    selectedItem?: Choice;
}
export declare class InputDropdown extends React.Component<InputDropdownProps, InputDropdownState> {
    constructor(props: InputDropdownProps);
    componentWillMount(): void;
    handleClickOpenList(): void;
    handleClickSelectionItem(selectedChoice: Choice): void;
    render(): JSX.Element;
}
