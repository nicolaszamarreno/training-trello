/// <reference types="react" />
import * as React from "react";
export interface MenuItem {
    iconClass?: string;
    color?: string;
    label?: string;
    onClick?: () => any;
}
export interface TooltipMenuProps {
    actionsList: MenuItem[];
    isOpen: boolean;
}
export declare class TooltipMenu extends React.Component<TooltipMenuProps, {}> {
    private menuList?;
    constructor(props: TooltipMenuProps);
    render(): JSX.Element;
    renderList(): JSX.Element;
    componentDidMount(): void;
    animElementList(): void;
}
