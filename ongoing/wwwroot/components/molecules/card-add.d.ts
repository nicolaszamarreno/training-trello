/// <reference types="react" />
import * as React from "react";
import { ObjectivesStore } from "../../stores/objectives-store";
export interface CardAddState {
    valueNewKeyResult: string;
}
export interface CardAddStore {
    objectivesStore?: ObjectivesStore;
    indexObjective: number;
    idObjective: number;
    onCancel: () => void;
}
export declare class CardAdd extends React.Component<CardAddStore, CardAddState> {
    private inputAdd;
    constructor(props: CardAddStore);
    onChange(event: React.FormEvent<HTMLTextAreaElement>): void;
    componentDidMount(): void;
    handleCardAddOnKeyDown(keyCap: React.KeyboardEvent<EventTarget>): void;
    saveKeyResult(): void;
    render(): JSX.Element;
}
