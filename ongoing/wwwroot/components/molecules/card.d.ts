/// <reference types="react" />
import * as React from "react";
import { KeyResult } from "../../stores/objectives-store";
export interface CardProps {
    id: number;
    index: number;
    indexObjective: number;
    keyResult: KeyResult;
    tag: string;
    tagColor: string;
    name: string;
    comments: string;
    deadline: string;
    onClick: (keyResult: KeyResult) => void;
}
export interface CardState {
    toggleModalCard: boolean;
}
export interface CardDNDProps extends CardProps {
    connectDragSource?: Function;
    connectDropTarget?: Function;
    connectDragPreview?: Function;
    isDragging?: boolean;
    change?: boolean;
    moveCard: (dragIndex: number, hoverIndex: number, idObjective: number) => void;
}
export declare class Card extends React.Component<CardDNDProps, {}> {
    constructor(props: CardDNDProps);
    render(): any;
}
