/// <reference types="react" />
import * as React from "react";
import { MenuItem } from "./tooltip-menu";
import { ButtonProps } from "../atoms/button";
export interface ButtonMenuProps extends ButtonProps {
    actionsList: MenuItem[];
}
export interface MenuItemIconsProps {
    iconClass: string;
    color?: string;
    label?: string;
    onClick?: () => any;
}
export interface ButtonMenuState {
    isOpen: boolean;
}
export declare class ButtonMenu extends React.Component<ButtonMenuProps, ButtonMenuState> {
    constructor(props: ButtonMenuProps);
    componentDidMount(): void;
    render(): JSX.Element;
    handleClick(): void;
}
