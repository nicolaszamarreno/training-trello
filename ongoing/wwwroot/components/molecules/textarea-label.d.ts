/// <reference types="react" />
import * as React from "react";
export interface TextareaLabelProps {
    inline?: boolean;
    idControl?: string;
    classComponent?: string;
    TextareaClassName?: string;
    TextareaPlaceholder?: string;
    onChange: () => void;
}
declare class TextareaLabel extends React.Component<TextareaLabelProps, {}> {
    constructor(props: TextareaLabelProps);
    render(): JSX.Element;
}
export { TextareaLabel };
