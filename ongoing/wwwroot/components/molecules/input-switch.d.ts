/// <reference types="react" />
import * as React from "react";
import { ChangeEvent } from "react";
export interface InputSwitchProps {
    type?: "checkbox" | "radio";
    value: boolean;
    title?: string;
    name?: string;
    label?: string;
    onChange: (value: boolean) => void;
}
export declare class InputSwitch extends React.Component<InputSwitchProps, {}> {
    changeCheckbox(event: ChangeEvent<HTMLInputElement>): void;
    render(): JSX.Element;
}
