/// <reference types="react" />
import * as React from "react";
export declare type InputTypes = "text" | "number" | "mail";
export interface InputProps {
    type: InputTypes;
    pattern?: string;
    componentClass?: string;
    idControl?: string;
    placeholder?: string;
    value?: string;
    required?: boolean;
    onChange?: () => void;
    onKeyPress?: () => void;
    inputValid?: boolean;
    warningMessage?: string;
    icon?: string;
    iconClass?: string;
}
declare class Input extends React.Component<InputProps, {}> {
    constructor(props: InputProps);
    renderMessage(): void;
    render(): JSX.Element;
}
export { Input };
