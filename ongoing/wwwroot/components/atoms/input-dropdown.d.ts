/// <reference types="react" />
import * as React from "react";
export interface Choice {
    label: string;
    icon?: string;
    active?: boolean;
}
export interface InputDropdownProps {
    choices: Choice[];
    label?: string;
}
export interface InputDropdownState {
    displayList: boolean;
    itemSelected?: Choice;
}
declare class InputDropdown extends React.Component<InputDropdownProps, InputDropdownState> {
    constructor(props: InputDropdownProps);
    componentWillMount(): void;
    handleClickOpenList(): void;
    handleClickSelectionItem(choiceSelected: Choice): void;
    render(): JSX.Element;
}
export { InputDropdown };
