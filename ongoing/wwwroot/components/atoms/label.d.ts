/// <reference types="react" />
import * as React from "react";
export interface LabelProps {
    idControl?: string;
    classComponent?: string;
}
declare class Label extends React.Component<LabelProps, {}> {
    render(): JSX.Element;
}
export { Label };
