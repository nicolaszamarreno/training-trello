/// <reference types="react" />
import * as React from "react";
export interface TextareaProps {
    componentClass?: string;
    idControl?: string;
    name?: string;
    placeholder?: string;
    onChange: () => void;
}
declare class Textarea extends React.Component<TextareaProps, {}> {
    constructor(props: TextareaProps);
    render(): JSX.Element;
}
export { Textarea };
