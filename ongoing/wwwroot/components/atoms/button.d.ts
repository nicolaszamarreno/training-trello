/// <reference types="react" />
import * as React from "react";
export declare type ButtonStatus = "default" | "success" | "danger" | "warning" | "disable";
export declare type ButtonSize = "small" | "medium" | "normal";
export declare type ButtonRole = "button" | "reset" | "submit";
export declare type ButtonStyle = "primary" | "secondary" | "round";
export declare type ButtonColor = "smart" | "stormtrooper";
export interface ButtonProps {
    style: ButtonStyle;
    size?: ButtonSize;
    label?: string;
    badgeValue?: number;
    status?: ButtonStatus;
    role?: ButtonRole;
    color?: ButtonColor;
    backgroundColor?: ButtonColor;
    borderColor?: ButtonColor;
    icon?: string;
    link?: string;
    componentClass?: string;
    onClick?: () => void;
}
export declare class Button extends React.Component<ButtonProps, {}> {
    constructor(props: ButtonProps);
    getClasses(): string[];
    render(): JSX.Element;
}
