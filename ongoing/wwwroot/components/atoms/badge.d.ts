/// <reference types="react" />
import * as React from "react";
export declare type BadgeType = "header" | "counter" | "date";
export interface BadgeProps {
    type: BadgeType;
    value?: number;
    componentClass?: string;
    color?: string;
    month?: string;
}
export declare class Badge extends React.Component<BadgeProps, {}> {
    constructor(props: BadgeProps);
    render(): JSX.Element;
}
