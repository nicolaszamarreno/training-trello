/// <reference types="react" />
import * as React from "react";
export interface MenuActionsProps {
    componentClass?: string;
    menuConfig?: ConfigButton;
}
export interface ConfigButton {
    componentClass?: string;
    borderColor?: string;
    color?: string;
    icon?: string;
}
export declare class MenuActions extends React.Component<MenuActionsProps, {}> {
    constructor(props: MenuActionsProps);
    render(): JSX.Element;
}
