/// <reference types="react" />
import * as React from "react";
export interface PopinProps {
    instruction?: string;
    title: string;
}
declare class Popin extends React.Component<PopinProps, {}> {
    constructor(props: PopinProps);
    render(): JSX.Element;
}
export { Popin };
