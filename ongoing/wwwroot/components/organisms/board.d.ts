/// <reference types="react" />
import * as React from "react";
import { ObjectivesStore } from "../../stores/objectives-store";
export interface BoardProps {
    objectivesStore?: ObjectivesStore;
}
export declare class Board extends React.Component<BoardProps, {}> {
    constructor(props: BoardProps);
    render(): JSX.Element;
}
