/// <reference types="react-dnd" />
import { KeyResult, ObjectivesStore } from "../../stores/objectives-store";
import { ColumnHeaderProps } from "../molecules/column-header";
export interface ColumnProps extends ColumnHeaderProps {
    idObjective: number;
    keyResults?: KeyResult[];
    objectivesStore?: ObjectivesStore;
}
export interface ColumnState {
    displayModal: boolean;
    addMode: boolean;
    keyResultIsSelected: KeyResult | null;
}
declare const _default: __ReactDnd.ContextComponentClass<ColumnProps>;
export default _default;
