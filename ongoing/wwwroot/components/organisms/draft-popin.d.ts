/// <reference types="react" />
import * as React from "react";
import { Moment } from "moment";
export interface PopinAddObjectiveProps {
    closePopin: () => void;
}
export interface PopinAddObjectiveState {
    fieldValid: boolean;
    startDate: Moment;
    objectiveTitle: string;
    objectiveDescription: string;
    objectiveType: string;
    targetPercent: number;
}
declare class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps);
    targetPercent(event: React.FormEvent<HTMLInputElement>): void;
    objectiveDescription(event: React.FormEvent<HTMLInputElement>): void;
    objectiveTitle(event: React.FormEvent<HTMLInputElement>): void;
    handleSubmit(event: React.FormEvent<HTMLInputElement>): void;
    handleChange(date: Moment): void;
    render(): JSX.Element;
}
export { PopinAddObjective };
