/// <reference types="react" />
import * as React from "react";
export interface ObjectiveTodoListState {
    inputValue: string;
}
export interface ObjectiveTodoListProps {
    classComponent?: string;
    tags: string[];
    onClickTask?: (item: string) => void;
}
export declare class AddTag extends React.Component<ObjectiveTodoListProps, ObjectiveTodoListState> {
    constructor(props: ObjectiveTodoListProps);
    render(): JSX.Element;
    handleKeyPress(event: KeyboardEvent): void;
    handleChange(event: React.FormEvent<HTMLInputElement>): void;
    renderList(): JSX.Element[];
}
