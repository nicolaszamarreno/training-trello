/// <reference types="react" />
import * as React from "react";
export interface ConfigColor {
    percent: number;
    color: string;
}
export interface ScaleLineProps {
    percentCursor: number;
    percentTarget: number;
    configColor: ConfigColor[];
    onMovePointer?: (value: number) => void;
}
export interface EventDrag {
    clientX: number;
    clientY: number;
}
export interface ScaleLineState {
    isTransition: boolean;
}
export declare class ScaleLine extends React.Component<ScaleLineProps, ScaleLineState> {
    private line;
    constructor(props: ScaleLineProps);
    render(): JSX.Element;
    getColor(): string;
    movePoint(event: Event & EventDrag): void;
}
