/// <reference types="react" />
import * as React from "react";
import { EmitterStore } from "../../stores/emitter-store";
import { ObjectivesStore } from "../../stores/objectives-store";
import { Moment } from "moment";
export interface PanelObjectiveState {
    title: string;
    description: string;
    startDate: Moment;
}
export interface PanelObjectiveProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}
export declare class PanelObjective extends React.Component<PanelObjectiveProps, PanelObjectiveState> {
    constructor(props: PanelObjectiveProps);
    changeTitle(event: React.FormEvent<HTMLInputElement>): void;
    changeDescription(event: React.FormEvent<HTMLInputElement>): void;
    getIndexPointer(value: number): void;
    handleSubmit(event: React.FormEvent<HTMLInputElement>): void;
    handleChange(date: Moment): void;
    render(): JSX.Element;
}
