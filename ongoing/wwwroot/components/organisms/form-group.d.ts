/// <reference types="react" />
import * as React from "react";
export interface FormGroupProps {
    onSubmit: () => void;
}
declare class FormGroup extends React.Component<FormGroupProps, {}> {
    constructor(props: FormGroupProps);
    render(): JSX.Element;
}
export { FormGroup };
