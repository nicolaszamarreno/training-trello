/// <reference types="react" />
import * as React from "react";
import { Moment } from "moment";
import { ObjectivesStore, KeyResult } from "../../stores/objectives-store";
export interface PopinAddObjectiveProps {
    closePopin: () => void;
    keyResult: KeyResult;
    objectivesStore?: ObjectivesStore;
}
export interface PopinAddObjectiveState {
    startDate: Moment;
    objectiveDescription: string;
    titleOnFocus: boolean;
}
export declare class PopinAddObjective extends React.Component<PopinAddObjectiveProps, PopinAddObjectiveState> {
    constructor(props: PopinAddObjectiveProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    handleInputChangeFocus(): void;
    handleChange(date: Moment): void;
    handleModalName(title: string): void;
    render(): JSX.Element;
}
