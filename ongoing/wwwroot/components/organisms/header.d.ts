/// <reference types="react" />
import * as React from "react";
import { EmitterStore } from "../../stores/emitter-store";
import { ObjectivesStore } from "../../stores/objectives-store";
export interface HeaderProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}
export declare class Header extends React.Component<HeaderProps, {}> {
    constructor(props: HeaderProps);
    fixedMenu(header: HTMLElement): void;
    render(): JSX.Element;
}
