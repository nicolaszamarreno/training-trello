/// <reference types="react" />
import * as React from "react";
export interface CommentState {
}
export interface CommentProps {
}
export declare class Comment extends React.Component<CommentProps, {}> {
    constructor(props: CommentProps);
    render(): JSX.Element;
}
