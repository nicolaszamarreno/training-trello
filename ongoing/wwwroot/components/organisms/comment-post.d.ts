/// <reference types="react" />
import * as React from "react";
export interface Comment {
    comment: string;
    author: string;
    time?: string;
    date?: string;
}
export interface CommentPostProps {
    comments: Comment[];
}
export declare class CommentPost extends React.Component<CommentPostProps, {}> {
    constructor(props: CommentPostProps);
    render(): JSX.Element;
}
