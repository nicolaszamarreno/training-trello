/// <reference types="react" />
import * as React from "react";
import { EmitterStore } from "../../stores/emitter-store";
export interface FilterProps {
    emitterStore?: EmitterStore;
}
export declare class Filter extends React.Component<FilterProps, {}> {
    constructor(props: FilterProps);
    render(): JSX.Element;
}
