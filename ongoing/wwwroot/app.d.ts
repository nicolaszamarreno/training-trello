/// <reference types="react" />
import * as React from "react";
import { EmitterStore } from "./stores/emitter-store";
import { ObjectivesStore } from "./stores/objectives-store";
export interface AppProps {
    emitterStore?: EmitterStore;
    objectivesStore?: ObjectivesStore;
}
export interface AppState {
    isLoading: boolean;
}
export declare class App extends React.Component<AppProps, AppState> {
    constructor(props: AppProps);
    componentDidMount(): void;
    render(): JSX.Element;
}
