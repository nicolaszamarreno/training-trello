export declare class EmitterStore {
    ModalObjectiveIsOpen: boolean;
    FilterIsOpen: boolean;
    ModalCardIsOpen: boolean;
    toggleModalObjective(): void;
    toggleFilter(): void;
    toggleModalCard(): void;
}
