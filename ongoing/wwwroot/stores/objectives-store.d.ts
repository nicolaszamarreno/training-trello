import { KeyResult } from "./objectives-store";
export interface KeyResult {
    id: number;
    title: string;
    tag: string;
    tagColor: string;
    deadline: string;
    comments: string;
    tags: string[];
}
export interface Objective {
    id: number;
    title: string;
    target: number;
    currentTarget: number;
    isPrivate: boolean;
    colorObjective: string;
    deadline?: Date;
    keyResults: KeyResult[];
}
export declare class ObjectivesStore {
    objectives: Objective[];
    changeOrderCard(dragIndex: number, hoverIndex: number, idObjective: number): void;
    getAllObjectives(): Promise<any>;
    addObjective(titleObjective: string, descriptionObjective: string, targetObjective: number): void;
    changeObjectivePercent(percent: number): void;
    SetObjectives(objectives: Objective[]): void;
    addKeyResult(objectiveId: number, title: string): void;
    changeNameOfKeyResult(keyResult: KeyResult, title: string): void;
}
