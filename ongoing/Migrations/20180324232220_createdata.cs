﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace ongoing.Migrations
{
    public partial class createdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ColorObjective",
                table: "Objectives",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CurrentTarget",
                table: "Objectives",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrivate",
                table: "Objectives",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ColorObjective",
                table: "Objectives");

            migrationBuilder.DropColumn(
                name: "CurrentTarget",
                table: "Objectives");

            migrationBuilder.DropColumn(
                name: "IsPrivate",
                table: "Objectives");
        }
    }
}
