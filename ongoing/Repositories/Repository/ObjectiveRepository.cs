﻿using Microsoft.EntityFrameworkCore;
using ongoing.Config;
using ongoing.Entites;
using ongoing.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Repositories.Repository
{
    public class ObjectiveRepository : IObjectiveRepository
    {
        private BddContext _bdd;

        public ObjectiveRepository(BddContext dbContext)
        {
            _bdd = dbContext;
        }

        public List<Objective> GetAllObjective()
        {
            return _bdd.Objectives.Include("KeyResults").ToList();
        }

        public Objective GetObjectiveById(long idObjective)
        {
            return _bdd.Objectives.FirstOrDefault(x => x.Id == idObjective);
        }

        public void UpdateObjective(Objective objectiveToUpdate)
        {
            _bdd.SaveChanges();
        }
    }
}
