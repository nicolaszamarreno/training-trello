﻿using ongoing.Config;
using ongoing.Entites;
using ongoing.Repositories.Interface;
using ongoing.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Repositories.Repository
{
    public class KeyResultRepository : IKeyResultRepository
    {
        private BddContext _bdd;

        public KeyResultRepository(BddContext dbContext)
        {
            _bdd = dbContext;
        }

        public KeyResult GetKeyResultById(long id)
        {
            return _bdd.KeyResults.FirstOrDefault(x => x.Id == id);
        }

        public long InsertKeyResult(KeyResult keyResult)
        {
            _bdd.KeyResults.Add(keyResult);
            var idNewEnter = _bdd.SaveChanges();

            return keyResult.Id;
        }
    }
}
