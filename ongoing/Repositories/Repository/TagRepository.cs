﻿using ongoing.Config;
using ongoing.Entites;
using ongoing.Repositories.Interface;
using ongoing.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Repositories.Repository
{
    public class TagRepository : ITagRepository
    {
        private BddContext _bdd;

        public TagRepository(BddContext dbContext)
        {
            _bdd = dbContext;
        }

        public List<Tag> GetAllTagsContainLetter(string enter)
        {
            return _bdd.Tags.Where(x => x.Title.Contains(enter)).ToList();
        }
    }
}
