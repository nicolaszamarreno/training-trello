﻿using ongoing.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Repositories.Interface
{
    public interface IObjectiveRepository
    {
        List<Objective> GetAllObjective();
        Objective GetObjectiveById(long idObjective);
        void UpdateObjective(Objective objectiveToUpdate);
    }
}
