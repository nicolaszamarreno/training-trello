﻿using ongoing.Entites;
using ongoing.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Repositories.Interface
{
    public interface ITagRepository
    {
        List<Tag> GetAllTagsContainLetter(string enter);
    }
}
