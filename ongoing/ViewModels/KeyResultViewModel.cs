﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.ViewModels
{
    public class KeyResultViewModel
    {
        public string Title { get; set; }
        public long IdObjective { get; set; }
    }
}
