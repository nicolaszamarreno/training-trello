﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ongoing.Entites;
using ongoing.Repositories.Interface;
using ongoing.Services.Interface;
using ongoing.ViewModels;

namespace ongoing.Controllers
{
    [Route("api/tag")]
    public class TagController : Controller
    {
        private ITagRepository _tagRepository;

        public TagController(ITagRepository tagRepository)
        {
            _tagRepository = tagRepository;
        }


        [HttpGet("{searchEnter}")]
        public List<Tag> Get(string searchEnter)
        {
            var result = _tagRepository.GetAllTagsContainLetter(searchEnter);

            return result;
        }
    }
}
