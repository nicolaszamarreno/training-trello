﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ongoing.Entites;
using ongoing.Repositories.Interface;
using ongoing.Services.Interface;
using ongoing.ViewModels;

namespace ongoing.Controllers
{
    [Route("api/keyresult")]
    public class KeyResultController : Controller
    {
        private IKeyResultRepository _keyResultRepository;
        private IKeyResultService _keyResultService;

        public KeyResultController(IKeyResultRepository keyResultRepository, IKeyResultService keyResultService)
        {
            _keyResultRepository = keyResultRepository;
            _keyResultService = keyResultService;
        }

        
        [HttpPost]
        public long Post([FromBody]KeyResultViewModel keyResultToAdd)
        {
            KeyResult keyResult = new KeyResult
            {
                Title = keyResultToAdd.Title
            };

            var idNewEnter = _keyResultService.EnterKeyResult(keyResultToAdd.IdObjective, keyResult);

            return idNewEnter;
        }
    }
}
