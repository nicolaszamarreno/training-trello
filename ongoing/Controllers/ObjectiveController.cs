﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ongoing.Entites;
using ongoing.Repositories.Interface;

namespace ongoing.Controllers
{
    [Route("api/objective")]
    public class ObjectiveController : Controller
    {
        private IObjectiveRepository _objectiveRepository;

        public ObjectiveController(IObjectiveRepository objectiveRepository)
        {
            _objectiveRepository = objectiveRepository;
        }

        // GET api/Objective
        [HttpGet]
        public List<Objective> GetAllObjective()
        {
            var result = _objectiveRepository.GetAllObjective();

            // Set 
            foreach(Objective objective in result)
            {
                if(objective.KeyResults == null)
                {
                    objective.KeyResults = new List<KeyResult>();
                }
            }

            return result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
