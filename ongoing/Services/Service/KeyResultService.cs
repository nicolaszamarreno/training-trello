﻿using ongoing.Entites;
using ongoing.Repositories.Interface;
using ongoing.Services.Interface;
using ongoing.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Services.Service
{
    public class KeyResultService : IKeyResultService
    {
        private IObjectiveRepository _objectiveRepository;
        private IKeyResultRepository _keyResultRepository;

        public KeyResultService(IObjectiveRepository objectiveRepository, IKeyResultRepository keyResultRepository)
        {
            _objectiveRepository = objectiveRepository;
            _keyResultRepository = keyResultRepository;
        }

        public long EnterKeyResult(long idObjective, KeyResult keyResult)
        {
            KeyResult newKeyResult = new KeyResult { Title = keyResult.Title };

            var idNewKeyResult = _keyResultRepository.InsertKeyResult(newKeyResult);
            var objectiveWillUdapte = _objectiveRepository.GetObjectiveById(idObjective);

            //Add New KeyResult in Objectives
            if(objectiveWillUdapte.KeyResults == null)
            {
                List<KeyResult> keyResultList = new List<KeyResult>();
                keyResultList.Add(newKeyResult);

                objectiveWillUdapte.KeyResults = keyResultList;
            }
            else
            {
                objectiveWillUdapte.KeyResults.Add(newKeyResult);
            }

            _objectiveRepository.UpdateObjective(objectiveWillUdapte);
            
            return idNewKeyResult;
        }
    }
}
