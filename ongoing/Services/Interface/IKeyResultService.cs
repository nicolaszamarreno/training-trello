﻿using ongoing.Entites;
using ongoing.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ongoing.Services.Interface
{
    public interface IKeyResultService
    {
        long EnterKeyResult(long idObjective, KeyResult keyResult);
    }
}
