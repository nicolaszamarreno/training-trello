# Training - OnGoing TrelloLike

_Just a little project for my training in C# & .Net Core. Allow to handle the Entity Framework and the Controllers for a API like Trello Like._

## Feature

*   Entity Framework SQLite
*   SQLite Files like Database
*   Framework Front
    *   React
    *   TypeScript
    *   MobX

## Let's Go !

### Front-End

You should install packages with NPM or Yarn

```bash
# ./ongoing/Client
$ yarn install
```

### Webpack

Launch these commands for compile, copy and watch yours files

```bash
# Copy and compile yours files
$ yarn run prod

# Watch yours files and compile
$ yarn run watch
```

## Migrations Database

```bash
# Create migration with name
$ dotnet ef migrations add InitialCreate

# Update database with entities
$ dotnet ef database update
```

_And more again, [see here](https://docs.microsoft.com/fr-fr/aspnet/core/data/ef-mvc/migrations)_
